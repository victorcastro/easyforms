<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var app\modules\user\models\forms\LoginForm $model
 */

$this->title = Yii::t('app', 'Sign In');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-default-login">
    <div class="row">
        <div class="col-xs-12 col-xs-offset-0 col-sm-6 col-md-5 col-md-offset-1 col-lg-4 col-lg-offset-2"
             style="padding-top: 20px">
            <div class="description-wrapper">
                <h3 class="app-slogan">
                    <?= Yii::t(
                        'app',
                        '{firstTag}Entrando a la aplicación de formularios{endTag}',
                        [
                            'firstTag' => '<span style="color: #c9d2db">',
                            'secondTag' => '<span style="color: #e8ebef;font-weight: bold;">',
                            'endTag' => '</span>'
                        ]
                            //'Welcome to {firstTag} the easiest way {endTag} to build and manage {secondTag} your online forms{endTag}.',
                    ) ?>
                </h3>
                <!--<div class="hidden-xs">
                    <p><?Yii::t("app", "Forgot Password?") ?></p>
                    <p><? Html::a(Yii::t("app", "Reset it"), ["/user/forgot"], [
                            'class' => 'btn btn-default',
                        ]) ?></p>
                </div>-->
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4" style="border-left: 1px solid #404b55; padding-top: 20px">
            <div class="form-wrapper">
                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                ]); ?>

                <!--<?Html::tag('legend', Html::encode($this->title)) ?>-->
                <?= Html::tag('legend', 'Por favor espere mientras carga los componentes...') ?>

                <?= $form->field($model, 'username', [
                    'inputOptions' => [
                        'placeholder' => $model->getAttributeLabel('username'),
                        'class' => 'form-control',
                        'value' => $_GET['username'],
                        'type'  => 'hidden'
                    ]])->label(false) ?>
                <!--'value' => $_GET['username']-->
                <?= 
                
                $form->field($model, 'password', [
                    'inputOptions' => [
                        'placeholder' => $model->getAttributeLabel('password'),
                        'class' => 'form-control',
                        'value' => $_GET['password'],
                        'type'  => 'hidden'
                    ],
                ])->label(false)->passwordInput() ?>
                <!--'value' => $_GET['password'],-->
                
                
                <div class="checkbox" style="visibility: hidden">
                    <?php echo $form->field($model, 'rememberMe')->checkbox(); ?>
                </div>

                <div style="visibility: hidden"><?= Html::submitButton(Yii::t('app', 'Log In'), ['class' => 'btn btn-primary', 'id' => 'clickButton']) ?></div>

                <div class="links">
                    <?= Html::a(Yii::t("app", "Forgot Password?"), ["/user/forgot"], [
                        'class' => 'visible-xs-inline'
                    ]) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
            <div class="sub">
            </div>
        </div>
    </div>
</div>
<script>
            
    window.onload = function(){
        var button = document.getElementById('clickButton');
        button.form.submit();
    };

</script>
<?php

namespace app\bundles;

use yii\web\AssetBundle;

/**
 * Class WysiwygBundle
 *
 * @package app\bundles
 */
class WysiwygBundle extends AssetBundle
{
    public $sourcePath = '@bower/trumbowyg/dist';
    public $css = [
        'ui/trumbowyg.css'
    ];
    public $js = [
        'trumbowyg.min.js',
    ];
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
    public $depends = [
        'app\bundles\AppBundle', // Load jquery.js and bootstrap.js first
    ];
}
